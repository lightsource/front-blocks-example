<?php

use LightSource\FrontBlocksExample\{
    Article\ArticleC,
    Header\HeaderC,
};
use LightSource\FrontBlocksFramework\{
    Blocks,
    Settings
};

require_once __DIR__ . '/vendors/vendor/autoload.php';

//// settings

ini_set('display_errors', 1);

$settings = new Settings();
$settings->setBlocksDirNamespace('LightSource\FrontBlocksExample');
$settings->setBlocksDirPath(__DIR__ . '/Blocks');
$settings->setErrorCallback(
    function (array $errors) {
        // todo log or any other actions
        echo '<pre>' . print_r($errors, true) . '</pre>';
    }
);
$blocks = new Blocks($settings);

//// usage

$headerController = new HeaderC();
$headerController->getModel()->loadByTest();

$articleController = new ArticleC();
$articleController->getModel()->loadByTest();

$content = $blocks->renderBlock($headerController);
$content .= $blocks->renderBlock($articleController);
$css     = $blocks->getUsedResources('.css', true);

//// html

?>
<html>

<head>

    <title>Example</title>
    <style>
        <?= $css ?>
    </style>
    <style>
        .article {
            margin-top: 10px;
        }
    </style>

</head>

<body>

<?= $content ?>

</body>

</html>
