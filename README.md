## What is it

It's an example of usage [FrontBlocksFramework](https://gitlab.com/lightsource/front-blocks-framework)

## How to use

1. Main file - **example.php**  
2. You can view the files within github or clone the repository (and install composer packages) and then have a look and test locally.  

Also the screenshot of the example.php output is attached.
