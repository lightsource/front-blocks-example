<?php

namespace LightSource\FrontBlocksExample\Button;

use LightSource\FrontBlocksFramework\Controller;

class ButtonC extends Controller {

	public function getModel(): ?Button {
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return parent::getModel();
	}

}
